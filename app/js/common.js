$(document).ready(function() {
    $('.wrapCategories').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 2000,
        centerMode:  true,
        dots: false
    });
    $('.sliderTShot').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $('.contantFirst').hover (function(){
        $('.firstSection').toggleClass('background');
    });

    var head = $('.header');
    if(head.offset().top >= 100) {
        $('.header').addClass('sticky');
        $('.text-slide').addClass('stickyLink');
        $('.logo').addClass('stickyLogo');
    }
    $(document).scroll (function(){
        if($(this).scrollTop() >= 100) {
            $('.header').addClass('sticky');
            $('.text-slide').addClass('stickyLink');
            $('.logo').addClass('stickyLogo');

        } else {
            $('.header').removeClass('sticky');
            $('.text-slide').removeClass('stickyLink');
            $('.logo').removeClass('stickyLogo');
        }
    });

    $( ".menu-icon" ).click(function() {
        $( this ).toggleClass( "effect1" );
        $('.wrapMenu').slideToggle();
    });

    $(function(){
        $('a[data-target^="anchor"]').bind('click', function(e){
            if($(document).width() < 1200) {
                $('.wrapMenu').slideUp(500);
                // $('.header').toggleClass('stickys');
            };

            var target = $(this).attr('href'),
                offset = $('.header').height() - (-40),
                bl_top = $(target).offset().top - offset-30;

            bl_top_correct = $(target).offset().top - offset - 60;
            bl_top_correct2 = $(target).offset().top - offset - 30;
            $('body, html').animate({scrollTop: bl_top}, 800);
            $('body, html').animate({scrollTop: bl_top_correct}, 200);
            $('body, html').animate({scrollTop: bl_top_correct2}, 300);
            e.preventDefault();
            return false;
        });
    });

    $(function(){
        $('a[data-target^="scrollDown"]').bind('click', function(e){

            var target = $(this).attr('href'),
                offset = $('.header').height() - 32,
                bl_top = $(target).offset().top - offset-30;

            bl_top_correct = $(target).offset().top - offset - 60;
            bl_top_correct2 = $(target).offset().top - offset - 30;
            $('body, html').animate({scrollTop: bl_top}, 800);
            $('body, html').animate({scrollTop: bl_top_correct}, 200);
            $('body, html').animate({scrollTop: bl_top_correct2}, 300);
            e.preventDefault();
            return false;
        });
    });

    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1200,
        autoplay: true,
        dots: true,
        arrow: true
    });


});
